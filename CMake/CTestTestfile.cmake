#
# Copyright (c) 2020, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
ADD_TEST(PlasComCMSystemTests "tests/system/autorun")
FILE(STRINGS @CTEST_SOURCE_DIRECTORY@/CMake/tests.list test_list_content)
FOREACH(testline ${test_list_content})
  STRING(REPLACE " " ";" test_list ${testline})
  LIST(GET test_list 0 test_name)
  LIST(GET test_list 1 test_keyword)
  LIST(GET test_list 2 test_filename)
  LIST(GET test_list 3 test_buildoption)
#  MESSAGE("Test Name: ${test_name}")
#  MESSAGE("Test Keyword: ${test_keyword}")
#  MESSAGE("Test Filename: ${test_filename}")
#  MESSAGE("Test Option: ${test_buildoption}")
  IF( "${test_buildoption}" STREQUAL "NONE")
    MESSAGE("Adding Test: ${test_name}") 
    ADD_TEST(${test_name} "tests/testresults" "${test_keyword}" "${test_filename}")
  ELSE()  
    FILE(STRINGS @CTEST_BINARY_DIRECTORY@/src/plascomcmconf.h test_present REGEX "#define ${test_buildoption} 1")
    IF(test_present) 
      MESSAGE("Adding Test: ${test_name}") 
      ADD_TEST(${test_name} "tests/testresults" "${test_keyword}" "${test_filename}")
    ENDIF()
  ENDIF()
ENDFOREACH()


